using System.Collections;
using UnityEngine;

public class Elevator : MonoBehaviour {
    private Animator animator;
    private bool closed = true;
    private GameObject exitArea;
    public void Awake() {
        animator = GetComponent<Animator>();
        Transform childTransform = transform.Find("ExitArea");
        if (childTransform != null) {
            exitArea = childTransform.gameObject;
            exitArea.SetActive(false);
            Debug.Log("Child object found: " + exitArea.name);
        } else {
            Debug.LogError("Child object 'ChildObjectName' not found!");
        }
    }

    public void Open() {
        animator.Play("Open", 0, 0f);
        closed = false;
        StartCoroutine(OpenRutine());
        exitArea.SetActive(true);
    }

    public void Close() {
        animator.Play("Close", 0, 0f);
        closed = true;
    }

    public void Interact() {
        if (closed) {
            Open();
        } else {
            Close();
        }
    }

    IEnumerator OpenRutine() {
        yield return new WaitForSeconds(0.5f);
    }
}
