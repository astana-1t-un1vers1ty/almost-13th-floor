﻿using UnityEngine;

namespace Assets.ScriptableObjects {
    [CreateAssetMenu(fileName = "PlayerStats", menuName = "ScriptableObjects/PlayerStats", order = 1)]
    public class PlayerStats : ScriptableObject {
        [Header("Stats")]
        [SerializeField] private float _health  = 100f;
        public string SpawnName = "Default";
        public bool Alive => _health > 0;

        public void TakeDamage(float damage) {
            if (Alive) {
                _health -= damage;
            }
            if (_health < 0) {
                _health = 0;
            }
        }
    }
}
