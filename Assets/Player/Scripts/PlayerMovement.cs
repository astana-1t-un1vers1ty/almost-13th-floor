using Assets.ScriptableObjects;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {
    public static PlayerMovement Instance;
    /// <summary>
    /// Default is 1f
    /// <para>The speed is multiplied by a factor</para>
    /// </summary>
    [SerializeField] private float movementSpeedFactor = 1f;
    /// <summary>
    /// Default is 4f
    /// <para>The speed is multiplied by a factor</para>
    /// </summary>
    [SerializeField] private float dashingSpeedFactor = 4f;
    /// <summary>
    /// Trail renderer
    /// </summary>
    [SerializeField] private TrailRenderer trailRenderer;

    [SerializeField] private PlayerStats playerStats;

    private PlayerControls playerControls;
    private Vector2 movementDirection;
    private Rigidbody2D rb;
    private Animator animator;
    private bool dashing;
    

    public void Awake() {
        PlayerPrefs.DeleteKey("SpawnPointName");
        DontDestroyOnLoad(gameObject);
        Instance = this;
        playerControls = new();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void Start() {
        string spawnPointName = PlayerPrefs.GetString("SpawnPointName", "Default");
        GameObject spawnPoint = GameObject.Find(spawnPointName);
        if (spawnPoint != null) {
            transform.position = spawnPoint.transform.position;
        } else {
            Debug.LogWarning("Spawn point not found: " + spawnPointName);
        }
        playerControls.Combat.Dash.performed += _ => Dash();
    }

    private void OnEnable() {
        playerControls.Enable();
    }

    private void Update() {
        movementDirection = GetMovementDirection();
    }

    private void FixedUpdate() {
        Move();
    }

    /// <summary>
    /// Get movement direction based on player controls
    /// </summary>
    /// <returns>Movement direction in Vector2</returns>
    private Vector2 GetMovementDirection() {
        var direction = playerControls.Movement.Move.ReadValue<Vector2>();
        animator.SetFloat("MoveX", direction.x);
        animator.SetFloat("MoveY", direction.y);
        return direction;
    }

    /// <summary>
    /// Changes position of Rigidbody2D
    /// </summary>
    private void Move() => rb.MovePosition(rb.position + movementDirection * (movementSpeedFactor * Time.fixedDeltaTime));

    private void Dash() {
        if (!dashing) {
            dashing = true;
            movementSpeedFactor *= dashingSpeedFactor;
            trailRenderer.emitting = true;
            StartCoroutine(EndDashRoutine());
        }
    }

    private IEnumerator EndDashRoutine() {
        var dashTime = 0.2f;
        var dashCD = 0.25f;
        yield return new WaitForSeconds(dashTime);
        movementSpeedFactor /= dashingSpeedFactor;
        trailRenderer.emitting = false;
        yield return new WaitForSeconds(dashCD);
        dashing = false;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        Debug.Log("Loaded scene: " + scene.name);
        // Get all respawns
        var respawns = GameObject.FindGameObjectsWithTag("Door").Select(door => {
            Transform exitArea = door.transform.Find("ExitArea");
            foreach (Transform child in exitArea.transform) {
                if (child.CompareTag("Respawn")) {
                    return child.gameObject;
                }
            }
            return null;
        }).Concat(GameObject.FindGameObjectsWithTag("Respawn"));

        string spawnPointName = PlayerPrefs.GetString("SpawnPointName", "DafaultSpawn");
        GameObject spawnPoint = respawns.FirstOrDefault(x => x.name == spawnPointName);
        if (spawnPoint != null) {
            // Assuming your player has a tag "Player"
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if (player != null) {
                player.transform.position = spawnPoint.transform.position;
            } else {
                Debug.LogError("Player not found in the scene.");
            }
        } else {
            Debug.LogWarning("Spawn point not found: " + spawnPointName);
        }
    }

    private void OnDestroy() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
