using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ExitArea : MonoBehaviour {
    public int sceneIndex;
    public string enterencePoint;

    public static UnityAction<string> onExit;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            Debug.Log("Player entered");
            SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
            PlayerPrefs.SetString("SpawnPointName", enterencePoint);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        Debug.Log("Player exited");
    }
}
