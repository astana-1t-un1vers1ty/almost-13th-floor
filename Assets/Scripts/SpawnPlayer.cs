using Cinemachine;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour {
    public GameObject playerPrefab;
    public GameObject vCamera;

    public void Spawn(Transform playerTransform) {
        if (!GameObject.FindGameObjectWithTag("Player")) {
            var player = Instantiate(playerPrefab, playerTransform.position, Quaternion.identity);
            DontDestroyOnLoad(player);
            var c = vCamera.GetComponentInChildren<ICinemachineCamera>();
            c.Follow = player.transform;
            var camera = Instantiate(vCamera, playerTransform.position, Quaternion.identity);
            DontDestroyOnLoad(camera);
        }
    }

    private void Start() {
        Spawn(transform);
    }
}
