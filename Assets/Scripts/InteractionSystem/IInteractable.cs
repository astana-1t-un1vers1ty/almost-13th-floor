﻿namespace Assets.Scripts.InteractionSystem {
    public interface IInteractable {
        void Interact();
    }
}
