﻿using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour {
    public bool inRange;
    public KeyCode keyCode;
    public UnityEvent interactAction;

    private void Update() {
        if (inRange && Input.GetKeyDown(keyCode)) {
            interactAction.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            inRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            inRange = false;
        }
    }
}
